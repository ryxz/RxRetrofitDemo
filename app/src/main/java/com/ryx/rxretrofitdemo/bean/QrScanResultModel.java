package com.ryx.rxretrofitdemo.bean;

/**
 * Author:YuanChao
 * Date:2018/10/12
 */
public class QrScanResultModel {
    private String brand_id;// "44",
    private String brand_num;// "9789787112038",
    private String brand_name;// "计算机教科书",
    private String brand_pm;// "书",
    private String brand_du;// "0",
    private String brand_gg;// "0",
    private String brand_jd;// "69",
    private String brand_tm;// "69",
    private String brand_jxw;// "69",
    private String brand_yhd;// "69",
    private String brand_jbz;// "69",
    private String brand_wem;// "69",
    private String show_type;// "0"

    public String getBrand_id() {
        return brand_id;
    }

    public void setBrand_id(String brand_id) {
        this.brand_id = brand_id;
    }

    public String getBrand_num() {
        return brand_num;
    }

    public void setBrand_num(String brand_num) {
        this.brand_num = brand_num;
    }

    public String getBrand_name() {
        return brand_name;
    }

    public void setBrand_name(String brand_name) {
        this.brand_name = brand_name;
    }

    public String getBrand_pm() {
        return brand_pm;
    }

    public void setBrand_pm(String brand_pm) {
        this.brand_pm = brand_pm;
    }

    public String getBrand_du() {
        return brand_du;
    }

    public void setBrand_du(String brand_du) {
        this.brand_du = brand_du;
    }

    public String getBrand_gg() {
        return brand_gg;
    }

    public void setBrand_gg(String brand_gg) {
        this.brand_gg = brand_gg;
    }

    public String getBrand_jd() {
        return brand_jd;
    }

    public void setBrand_jd(String brand_jd) {
        this.brand_jd = brand_jd;
    }

    public String getBrand_tm() {
        return brand_tm;
    }

    public void setBrand_tm(String brand_tm) {
        this.brand_tm = brand_tm;
    }

    public String getBrand_jxw() {
        return brand_jxw;
    }

    public void setBrand_jxw(String brand_jxw) {
        this.brand_jxw = brand_jxw;
    }

    public String getBrand_yhd() {
        return brand_yhd;
    }

    public void setBrand_yhd(String brand_yhd) {
        this.brand_yhd = brand_yhd;
    }

    public String getBrand_jbz() {
        return brand_jbz;
    }

    public void setBrand_jbz(String brand_jbz) {
        this.brand_jbz = brand_jbz;
    }

    public String getBrand_wem() {
        return brand_wem;
    }

    public void setBrand_wem(String brand_wem) {
        this.brand_wem = brand_wem;
    }

    public String getShow_type() {
        return show_type;
    }

    public void setShow_type(String show_type) {
        this.show_type = show_type;
    }
}
