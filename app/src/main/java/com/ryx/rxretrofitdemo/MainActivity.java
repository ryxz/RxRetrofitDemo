package com.ryx.rxretrofitdemo;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.ryx.rxretrofitdemo.base.BaseActivity;
import com.ryx.rxretrofitdemo.base.BaseSubscriber;
import com.ryx.rxretrofitdemo.base.BasicModel;
import com.ryx.rxretrofitdemo.bean.GgInfoBean;
import com.ryx.rxretrofitdemo.bean.QrScanResultModel;
import com.tbruyelle.rxpermissions2.RxPermissions;
import com.uuzuche.lib_zxing.activity.CaptureActivity;
import com.uuzuche.lib_zxing.activity.CodeUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends BaseActivity implements View.OnClickListener{
    private TextView deviceText;
    private TextView ggText;
    private Button startButton;
    public static final String IMEI = "1234567890";
    private static final String TAG = "MainActivity";
    public static final int REQUEST_CODE = 111;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        deviceText = (TextView) findViewById(R.id.device_id);
        ggText = (TextView) findViewById(R.id.gg_info_text);
        startButton = (Button) findViewById(R.id.start_button);
        startButton.setOnClickListener(this);
//        ggText.setOnClickListener(this);
//        getAdvertId();
    }

//    private void getAdvertId() {
//        Map<String,String> map = new HashMap<>();
//        map.put("device_id",IMEI);
//        getHttpService().getDeviceId(map)
//                .compose(this.<BasicModel<String>>apply())
//                .subscribe(new BaseSubscriber<BasicModel>(this) {
//                    @Override
//                    protected void onDoNext(BasicModel basicModel) {
//
//                            String message = basicModel.getMessage();
//                            String data = (String) basicModel.getData();
//                            Log.e(TAG,data);
//                            Log.e(TAG,message);
//                            deviceText.setText(data);
//                            getGg(data);
//                        }
//
//                });
//    }

//    private void getGg(String deviceId) {
//        Map<String,String> mMap = new HashMap<>();
//        mMap.put("device_id",IMEI);
//        mMap.put("d_advert_id",deviceId);
//        getHttpService().getInfo(mMap).compose(this.<BasicModel<GgInfoBean>>apply())
//                .subscribe(new BaseSubscriber<BasicModel<GgInfoBean>>(this) {
//                    @Override
//                    protected void onDoNext(BasicModel<GgInfoBean> ggInfoBeanBasicModel) {
//                        Log.e(TAG,ggInfoBeanBasicModel.getMessage());
//                        Log.e(TAG,ggInfoBeanBasicModel.getData().getA_name());
//                        List<String> mList = new ArrayList<>();
//                        mList = ggInfoBeanBasicModel.getData().getA_image2();
//                        for (int i = 0;i<mList.size();i++) {
//                            ggText.setText(mList.get(i));
//                        }
//                    }
//                });
//    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.start_button:
//                requestPermission();
//                Intent intent = new Intent(MainActivity.this, CaptureActivity.class);
//                startActivityForResult(intent,REQUEST_CODE);
                new RxPermissions(this).request(Manifest.permission.CAMERA)
                        .subscribe(new BaseSubscriber<Boolean>() {
                            @Override
                            protected void onDoNext(Boolean aBoolean) {
                                if (aBoolean) {
                                    Intent intent = new Intent(MainActivity.this, CaptureActivity.class);
                                    startActivityForResult(intent,REQUEST_CODE);
                                } else {
                                    Toast.makeText(MainActivity.this,"请打开相机权限！",Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
        }
    }
    private void requestPermission() {
        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.M){
            if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.CAMERA)!= PackageManager.PERMISSION_GRANTED){
                ActivityCompat.requestPermissions(MainActivity.this,new String[]{Manifest.permission.CAMERA},1);
            }
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE) {
            if (data != null) {
                Bundle bundle = data.getExtras();
                if (bundle == null) {
                    return;
                }
                if (bundle.getInt(CodeUtils.RESULT_TYPE) == CodeUtils.RESULT_SUCCESS) {
                    String result = bundle.getString(CodeUtils.RESULT_STRING);
//                    ggText.setText(result);
                    Intent intent = new Intent();
                    intent.setAction("android.intent.action.VIEW");
                    Uri uri = Uri.parse(result);
                    intent.setData(uri);
                    startActivity(intent);
//                    resultText.setText(result);
//                    getHttpService().qrCode("ios","scan",result)
//                            .compose(this.<BasicModel<QrScanResultModel>>apply())
//                            .subscribe(new BaseSubscriber<BasicModel<QrScanResultModel>>() {
//                                @Override
//                                protected void onDoNext(BasicModel<QrScanResultModel> qrScanResultModelBasicModel) {
//
//                                }
//                            });
                    Toast.makeText(MainActivity.this,"解析结果:" + result,Toast.LENGTH_SHORT);
                    Log.e(TAG,"解析结果----------------------------:" +result);

                } else if (bundle.getInt(CodeUtils.RESULT_TYPE) == CodeUtils.RESULT_FAILED){
                    Toast.makeText(MainActivity.this,"解析二维码失败",Toast.LENGTH_SHORT).show();
                }
            }
        }
    }
}
