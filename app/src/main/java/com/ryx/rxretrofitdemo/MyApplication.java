package com.ryx.rxretrofitdemo;

import android.app.Application;
import android.content.SharedPreferences;

import com.uuzuche.lib_zxing.activity.ZXingLibrary;

/**
 * Created by Ryx on 2019/1/7.
 */
public class MyApplication extends Application {
    private static MyApplication context;

    public static MyApplication getInstance() {

        return context;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        ZXingLibrary.initDisplayOpinion(this);
    }
}
