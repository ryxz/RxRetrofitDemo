package com.ryx.rxretrofitdemo.http;


import com.ryx.rxretrofitdemo.base.BasicModel;
import com.ryx.rxretrofitdemo.bean.GgInfoBean;
import com.ryx.rxretrofitdemo.bean.QrScanResultModel;

import java.util.HashMap;
import java.util.Map;


import io.reactivex.Observable;
import io.reactivex.annotations.Nullable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;

public interface HttpService {
//    String IP = "http://47.96.225.99/bill/";
        String IP = "http://47.97.211.84/index.php";




    /**
     * 获取模板id
     *
     * @param
     * @return
     */
    @FormUrlEncoded
    @POST("public/api/adver/model")
    Observable<BasicModel<String>> getDeviceId(@FieldMap Map<String,String> fileds);

    /**
     * 获取广告信息
     * @param
     * @param
     * @return
     */
    @FormUrlEncoded
    @POST("public/api/adver/advertise")
    Observable<BasicModel<GgInfoBean>> getInfo(@FieldMap Map<String,String> ggInfo);

    /**
     * 用户注册获取验证码
     *
     * @return
     */
    @GET("index.php")
    Observable<BasicModel> regSendMsgCode1(@Query("c") String c, @Query("a") String a,
                                           @Query("member_mobile") String phone);


    /**
     * 用户注册
     *
     * @param phone
     * @param verify
     * @return
     */
    @GET("index.php")
    Observable<BasicModel> register(@Query("c") String c, @Query("a") String a,
                                    @Query("verification") String verify,
                                    @Query("account") String phone,
                                    @Nullable @Query("parent_member_id") String pmi,
                                    @Query("member_passwd") String psd);


    /**
     * 用户登录发送验证码
     *
     * @param phone
     * @return
     */
    @GET("index.php")
    Observable<BasicModel> sendLoginMsg1(@Query("c") String c, @Query("a") String a,
                                         @Query("member_mobile") String phone);

//    /**
//     * 用户验证码登录
//     *
//     * @param phone
//     * @param verify
//     * @return
//     */
//    @GET("index.php")
//    Observable<BasicModel<UserInfoModel>> msgLogin1(@Query("c") String c, @Query("a") String a,
//                                                    @Query("account") String phone, @Query("verification") String verify);
//
//
//    /**
//     * 商品搜索
//     * 参数名称是否必须数据类型默认值描述
//     * goods_name false string 商品名称
//     * gc_type false string 商品分类
//     * brand false string 品牌
//     *
//     * @return
//     */
//    @GET("user/search")
//    Observable<BasicModel<List<SearchResultModel>>> search(@QueryMap HashMap<String, String> params);
//
//
//    /**
//     * user_id true number 用户id
//     * invoice_type true string 0 类型(0个人发票
//     * )(1单位发票)
//     * invoice_name true string 发票抬头
//     * username true string 联系人
//     * phone true number 联系电话
//     * address true string 联系地址
//     * invoice_manage true string 0 状态 0随货寄送
//     * ，1单独寄送
//     *
//     * @param user_id
//     * @param invoice_type
//     * @param invoice_name
//     * @param invoice_manage
//     * @return
//     */
//    @GET("user/fadd")
//    Observable<BasicModel<List<GoodDetailModel>>> addTicket(@Query("user_id") String user_id,
//                                                            @Query("invoice_type") String invoice_type,
//                                                            @Query("invoice_name") String invoice_name,
//                                                            @Query("username") String username,
//                                                            @Query("phone") String phone,
//                                                            @Query("invoice_manage") String invoice_manage,
//                                                            @Query("address") String address,
//                                                            @Query("duty") String duty
//    );
//
//    /**
//     * 获取发票列表
//     *
//     * @param user_id
//     * @param invoice_type 0,个人普通发票1,单位普通发票3,所有发票
//     * @return
//     */
//    @GET("user/invoice")
//    Observable<BasicModel<List<InvoiceModel>>> getInvoiceList(@Query("user_id") String user_id,
//                                                              @Query("invoice_type") String invoice_type);
//
//    /**
//     * 接口名称:发票删除
//     *
//     * @param invoice_id
//     * @return
//     */
//    @GET("user/delete")
//    Observable<BasicModel> deleteInvoice(@Query("invoice_id") String invoice_id);
//
//    /**
//     * 接口名称:发票信息修改
//     *
//     * @param invoice_id
//     * @return
//     */
//    @GET("user/gedit")
//    Observable<BasicModel> editInvoice(@Query("invoice_id") String invoice_id,
//                                       @Query("user_id") String user_id,
//                                       @Query("invoice_type") String invoice_type,
//                                       @Query("invoice_name") String invoice_name,
//                                       @Query("username") String username,
//                                       @Query("phone") String phone,
//                                       @Query("invoice_manage") String invoice_manage,
//                                       @Query("address") String address,
//                                       @Query("duty") String duty
//    );

    /**
     * user_id true number 用户id
     * nickname true string 昵称 （选填）
     * user_sex true string 性别(0男)(1女
     * )(3保密) （选填）
     * brithday true string 出生日期 只能修改
     * 一次 （选填）
     *
     * @return
     */
    @GET("user/uedit")
    Observable<BasicModel> userEdit(@QueryMap() Map<String, String> params);


    @Multipart
    @POST("news/upload")
    Observable<BasicModel> uploadAvatar(@PartMap Map<String, RequestBody> partMap);


//    /**
//     * 接口名称:查看个人地址
//     *
//     * @param user_id
//     * @return
//     */
//    @GET("news/address")
//    Observable<BasicModel<List<AddressModel>>> getAddress(@Query("user_id") String user_id);

    /**
     * 参数名称 是否必须 数据类型 默认值 描述
     * user_id true number 用户id
     * user_name true string 用户姓名
     * sex true number 性别
     * total_address true string 总地址
     * address true string 具体地址
     * user_phone true number 手机电话
     * is_default true number 1默认收货
     * label true number 标签 0(家
     * ), 2(酒店)
     * call true number 固定电话
     * province true string 省
     * city true string 市
     * atea true string 区
     *
     * @return
     */
    @GET("news/addAddress")
    Observable<BasicModel> addAddress(@QueryMap Map<String, String> map);

    /**
     * 编辑地址。（仅比{@link #addAddress(Map)}多了一个address_id参数）
     *
     * @param map
     * @return
     */
    @GET("news/upAddress")
    Observable<BasicModel> editAddress(@QueryMap Map<String, String> map);

    /**
     * 删除地址。
     *
     * @param addressId
     * @return
     */
    @GET("news/deleteAddress")
    Observable<BasicModel> deleteAddress(@Query("address_id") String addressId);

    /**
     * user_id true string 会员ID
     * user_name true string 会员姓名
     * goods_name true string 商品名称
     * goods_image true string 商品主图片
     * gid true string 商品ID
     * coll_price true string 商品收藏时价格
     * coll_msg
     *
     * @return
     */
    @GET("news/collect")
    Observable<BasicModel> addCollection(@Query("user_id") String user_id,
                                         @Query("user_name") String user_name,
                                         @Query("goods_name") String goods_name,
                                         @Query("goods_image") String goods_image,
                                         @Query("gid") String gid,
                                         @Query("coll_price") String coll_price,
                                         @Query("coll_msg") String coll_msg

    );

    /**
     * 删除收货地址。
     *
     * @param addressId
     * @return
     */
    @GET("index.php")
    Observable<BasicModel> deleteAddress(@Query("c") String c, @Query("a") String a,
                                         @Query("address_id") String addressId);

    /**
     * 修改收货地址
     * address_id true string
     * member_id true string 用户id
     * true_name true string 收货姓名
     * sex true string 性别
     * address true string 收货地址
     * mob_phone true string 手机号码
     * label true string 标签 0(家) ,1(公司), 2(酒店)
     */
    @GET("index.php")
    Observable<BasicModel> editAddress1(@Query("c") String c, @Query("a") String a,
                                        @Query("address_id") String address_id,
                                        @Query("member_id") String member_id,
                                        @Query("true_name") String true_name,
                                        @Query("sex") String sex,
                                        @Query("address") String address,
                                        @Query("mob_phone") String mob_phone,
                                        @Query("label") String label);


//    /**
//     * 获取收藏列表
//     *
//     * @param user_id
//     * @return
//     */
//    @GET("news/userCollect")
//    Observable<BasicModel<List<CollectionModel>>> getCollection(@Query("user_id") String user_id);

    /**
     * @return
     */
    @GET("news/delCollect")
    Observable<BasicModel> deleteCollection(@Query("gid") String coll_id, @Query("user_id") String userId);

//    /**
//     * 提交订单
//     *
//     * @param requestModel
//     * @return
//     */
//    @POST("news/order")
//    Observable<BasicModel<String>> order(@Body OrderRequestModel requestModel);

//    /**
//     * 优惠券展示
//     *
//     * @return
//     */
//    @GET("news/receiveCoupon")
//    Observable<BasicModel<List<CouponModel>>> receiveCoupon();

    /**
//     * 用户优惠券
//     *
//     * @return
//     */
//    @GET("news/userCoupon")
//    Observable<BasicModel<List<CouponModel>>> userCoupon(@Query("user_id") String user_id, @Query("type") String type);


    /**
//     * @param user_id
//     * @param order_state 0,所有订单 10,待
//     *                    付款 20，代发货
//     *                    30，已发货
//     * @return
//     */
//    @GET("news/seeOrder")
//    Observable<BasicModel<List<OrderModel>>> seeOrder(@Query("user_id") String user_id, @Query("order_state") String order_state);

    /**
     * 精品
     *
     * @return
     */
    @GET("news/quality")
    Observable<BasicModel<String>> jingpin();

//    /**
//     * 查看红包
//     *
//     * @return
//     */
//    @GET("news/seeRed")
//    Observable<BasicModel<List<RedBagModel>>> seeRed();

    /**
     * 登录红包领取
     *
     * @param user_id
     * @param money
     * @return
     */
    @GET("news/redEnvelopes")
    Observable<BasicModel> redEnvelopes(@Query("user_id") String user_id,
                                        @Query("money") String money);

//    /**
//     * :个人红包展示
//     *
//     * @param user_id
//     * @param red_state 红包状态 1，可用
//     *                  0是过期
//     * @return
//     */
//    @GET("news/red")
//    Observable<BasicModel<List<ReceivedRedBagModel>>> myRedBag(@Query("user_id") String user_id,
//                                                               @Query("red_state") String red_state);


    /**
     * 确认收货
     *
     * @param user_id
     * @param order_sn
     * @return
     */
    @GET("news/confirm")
    Observable<BasicModel> confirm(@Query("user_id") String user_id, @Query("order_sn") String order_sn);

    /**
     * user_id
     * gid
     * money
     * order_sn
     * sale_message
     * goods_name
     * goods_picture
     * logistics_company
     * logistics_number
     * refunds
     *
     * @param user_id
     * @param order_sn
     * @return
     */
    @GET("news/afterSale")
    Observable<BasicModel> saleAfter(@Query("user_id") String user_id,
                                     @Query("gid") String gid,
                                     @Query("money") String money,
                                     @Query("order_sn") String order_sn,
                                     @Query("sale_message") String sale_message,
                                     @Query("goods_name") String goods_name,
                                     @Query("goods_picture") String goods_picture,
                                     @Query("refunds") String refunds
    );


//    /**
//     * 查看个人申请信息
//     *
//     * @param user_id
//     * @return
//     */
//    @GET("news/seeAfterSale")
//    Observable<BasicModel<List<SaleAfterModel>>> seeSaleAfter(@Query("user_id") String user_id);

//    /**
//     * 账号登录
//     *
//     * @param phone
//     * @param pwd
//     * @return
//     */
//    @GET("user/lot")
//    Observable<BasicModel<UserInfoModel>> accountLogin(@Query("phone") String phone, @Query("passwd") String pwd);


//    /**
//     * 通知中心
//     *
//     * @param url
//     * @return
//     */
//    @GET
//    Observable<BasicModel<List<NoticeModel>>> orderForm(@Url String url);

//    /**
//     * 领券中心
//     *
//     * @param gc_id
//     * @param member_id
//     * @return
//     */
//    @GET("index.php?c=ios&a=quanall")
//    Observable<BasicModel<List<CouponCenterModel>>> getCoupons(@Query("gc_id") String gc_id, @Query("member_id") String member_id);


    /**
     * 领取优惠券
     *
     * @param member_id            会员id
     * @param voucher_t_id         代金券的id
     * @param voucher_t_desc       代金券模版描述
     * @param voucher_t_title      代金券模版名称
     * @param voucher_t_start_date 代金券模版有效期开始时间
     * @param voucher_t_end_date   代金券模版有效期结束时间
     * @param voucher_t_price      代金券模版面额
     * @param voucher_t_giveout    模版已发放的代金券数量
     * @param voucher_t_limit      代金券使用时的订单限额
     * @param voucher_t_type       代金券的类别
     * @param voucher_t_vid        代金券的店铺id
     * @return
     */
    @GET("index.php?c=ios&a=receives")
    Observable<BasicModel> getCoupon(@Query("member_id") String member_id
            , @Query("voucher_t_id") String voucher_t_id
            , @Query("voucher_t_desc") String voucher_t_desc
            , @Query("voucher_t_title") String voucher_t_title
            , @Query("voucher_t_start_date") String voucher_t_start_date
            , @Query("voucher_t_end_date") String voucher_t_end_date
            , @Query("voucher_t_price") String voucher_t_price
            , @Query("voucher_t_giveout") String voucher_t_giveout
            , @Query("voucher_t_limit") String voucher_t_limit
            , @Query("voucher_t_type") String voucher_t_type
            , @Query("voucher_t_vid") String voucher_t_vid
    );

    /**
     * 修改密码发送验证码
     *
     * @param user_phone
     * @return
     */
    @GET("user/verify")
    Observable<BasicModel> editPwdMsgCode(@Query("user_phone") String user_phone);

    /**
     * 修改密码发送验证码
     *
     * @param user_phone
     * @return
     */
    @GET("index.php")
    Observable<BasicModel> editPwdMsgCode1(@Query("c") String c, @Query("a") String a,
                                           @Query("member_mobile") String user_phone);

    /**
     * 忘记密码，重设密码
     *
     * @param user_phone
     * @param passwd
     * @param verify
     * @return
     */
    @GET("index.php")
    Observable<BasicModel> forgetPwd1(@Query("c") String c, @Query("a") String a,
                                      @Query("verification") String verify,
                                      @Query("account") String user_phone,
                                      @Query("member_passwd") String passwd
    );

    /**
     * 转盘
     *
     * @param user_id
     * @return
     */
    @GET("news/prize_arr")
    Observable<BasicModel<String>> prize_arr(@Query("user_id") String user_id);

//    @GET("user/category")
//    Observable<BasicModel<List<CategoryModel>>> category();
//
//    /**
//     * 产品首页(获取产品所有信息)
//     *
//     * @return
//     */
//    @GET()
//    Observable<BasicModel<List<HomeDataModel>>> getHomeData(@Url String url);
//
//    @GET()
//    Observable<BasicModel<List<HomeDataModel>>> getHomeRecommend(@Url String url);
//
//    /**
//     * 商铺首页
//     *
//     * @return
//     */
//    @GET("index.php")
//    Observable<BasicModel<List<ShopFragment.ShopModel>>> getShopHomeData(@QueryMap Map<String, String> map);

//    /**
//     * 特卖处理
//     *
//     * @param url
//     * @return
//     */
//    @GET("index.php")
//    Observable<BasicModel<List<PromotionGoodModel>>> getPromotionData(@QueryMap Map<String, String> url);
//
//    /**
//     * 积分商城首页数据
//     *
//     * @param url
//     * @return
//     */
//    @GET
//    Observable<BasicModel<List<AccumulateMode>>> getAccuHomeData(@Url String url);
//
//    /**
//     * 分类列表数据。
//     *
//     * @return
//     */
//    @GET("index.php?c=ios")
//    Observable<BasicModel<List<HomeCategoryDetailModel>>> getHomeCategoryDetailList(@Query("a") String a,
//                                                                                    @Query("type_id") String typeId,
//                                                                                    @Query("key") String key);
//
//    /**
//     * 分类列表-搜索
//     *
//     * @param url
//     * @return
//     */
//    @GET
//    Observable<BasicModel<List<HomeCategoryDetailModel>>> getHomeCategorySearch(@Url String url);
//
//    /**
//     * 分类列表-降序。。奇葩的接口。
//     *
//     * @param url
//     * @return
//     */
//    @GET
//    Observable<BasicModel<List<HomeCategoryDetailModel>>> getHomeCategoryDesc(@Url String url);
//
//    /**
//     * 筛选
//     *
//     * @param url
//     * @return
//     */
//    @GET
//    Observable<BasicModel<List<HomeCategoryDetailModel>>> filter(@Url String url);
//
//
//    @GET
//    Observable<BasicModel<ShopHomeListModel>> getShopGoodList(@Url String url);
//
//    /**
//     * 积分商城搜索-筛选
//     *
//     * @param url
//     * @return
//     */
//    @GET
//    Observable<BasicModel<List<AccumulateFilterSearchModel>>> accumulateSearchFilter(@Url String url);
//
//    /**
//     * 积分商品详情
//     *
//     * @param url
//     * @return
//     */
//    @GET
//    Observable<BasicModel<GoodDetailModel>> accumulateGoodDetail(@Url String url);


    /**
     * 添加购物车。
     *
     * @param map
     * @return
     */
    @GET("index.php")
    Observable<BasicModel> addCart(@QueryMap Map<String, String> map);


//    /**
//     * 购物车列表
//     *
//     * @param c
//     * @param a
//     * @param member_id
//     * @return
//     */
//    @GET("index.php")
//    Observable<BasicModel<List<CarModel>>> queryCar(@Query("c") String c, @Query("a") String a,
//                                                    @Query("member_id") String member_id);
//
//    /**
//     * 个人中心
//     *
//     * @return
//     */
//    @GET
//    Observable<BasicModel<List<PersonalModel>>> getUserCenter(@Url String url);


    /**
     * 修改头像
     *
     * @return
     */
    @Multipart
    @POST("index.php?c=ios&a=headportrait")
    Observable<BasicModel> SetHeadPortrait(@Part("member_id") RequestBody member_id,
                                           @Part MultipartBody.Part member_avatar);


    /**
     * 通过旧密码修改密码
     *
     * @param url
     * @return
     */
    @GET
    Observable<BasicModel> revampPwd(@Url String url);


    /**
     * 通过手机号码修改密码
     *
     * @param url
     * @return
     */
    @GET
    Observable<BasicModel> phoneChange(@Url String url);

    /**
     * 获取验证码
     *
     * @param url
     * @return
     */
    @GET
    Observable<BasicModel> sendCodeSms(@Url String url);

    /**
     * 修改用户名
     *
     * @param url
     * @return
     */
    @GET
    Observable<BasicModel> memberName(@Url String url);


    @FormUrlEncoded
    @POST("index.php?c=ios&a=newComment")
    Observable<BasicModel> submitEvaluate1(@Field("order_sn") String order_sn,
                                           @Field("member_id") String member_id,
                                           @Field("rider_id") String rider_id,
                                           @Field("vid") String vid,
                                           @Field("geval_scores_a") String geval_scores_a,
                                           @Field("geval_scores_c") String geval_scores_c,
                                           @Field("geval_content_a") String geval_content_a,
                                           @Field("geval_content_c") String geval_content_c,
                                           @Field("data") String data);

    /**
     * 修改默认地址
     *
     * @param member_id
     * @param address_id
     * @return
     */
    @GET("index.php")
    Observable<BasicModel> changeDefaultAddress(@Query("c") String c, @Query("a") String a,
                                                @Query("member_id") String member_id,
                                                @Query("address_id") String address_id
    );


//    /**
//     * 订单详情
//     *
//     * @param order_sn
//     * @return
//     */
//    @GET("index.php?c=ios&a=pinlun")
//    Observable<BasicModel<List<EvaluateModel>>> orderDetails1(@Query("order_sn") String order_sn);
//
//    /**
//     * 接口名称:查看个人收货地址
//     *
//     * @param member_id
//     * @return
//     */
//    @GET("index.php")
//    Observable<BasicModel<List<AddressModel>>> getAddress(@Query("c") String c, @Query("a") String a,
//                                                          @Query("member_id") String member_id);

    /**
     * 新增收货地址
     * member_id true string 用户id
     * true_name true string 收货姓名
     * sex true string 性别
     * address true string 收货地址
     * mob_phone true string 手机号码
     * label true string 标签 0(家) ,1(公司), 2(酒店)
     */
    @GET("index.php")
    Observable<BasicModel> addAddress1(@Query("c") String c, @Query("a") String a,
                                       @Query("member_id") String member_id,
                                       @Query("true_name") String true_name,
                                       @Query("sex") String sex,
                                       @Query("address") String address,
                                       @Query("mob_phone") String mob_phone,
                                       @Query("label") String label);


    /**
     * 帮我开店
     *
     * @return
     */
    @GET("index.php?c=ios")
    Observable<BasicModel> openShop(@Query("a") String openShop,
                                    @Query("member_id") String member_id,
                                    @Query("user_name") String user_name,
                                    @Query("user_tel") String user_tel,
                                    @Query("user_message") String user_message,
                                    @Query("user_address") String user_address);


    /**
     * 城市代理
     *
     * @param member_id
     * @param cityagent_name
     * @param cityagent_tel
     * @param cityagent_address
     * @param cityagent_message
     * @return
     */
    @FormUrlEncoded
    @POST("index.php?c=ios&a=cityagent")
    Observable<BasicModel> cityagent(@Field("member_id") String member_id,
                                     @Field("cityagent_name") String cityagent_name,
                                     @Field("cityagent_tel") String cityagent_tel,
                                     @Field("cityagent_address") String cityagent_address,
                                     @Field("cityagent_message") String cityagent_message);


    /**
     * 支付宝提现
     *
     * @param member_id
     * @param money
     * @param account
     * @return
     */
    @FormUrlEncoded
    @POST("index.php?c=ios&a=aliRecode")
    Observable<BasicModel> aliRecode(@Field("member_id") String member_id,
                                     @Field("money") String money,
                                     @Field("account") String account);

    /**
     * 配送加盟
     *
     * @param service_name
     * @param service_tel
     * @param service_address
     * @param service_message
     * @return
     */
    @FormUrlEncoded
    @POST("index.php?c=ios&a=service")
    Observable<BasicModel> service(@Field("service_name") String service_name,
                                   @Field("service_tel") String service_tel,
                                   @Field("service_address") String service_address,
                                   @Field("service_message") String service_message);

    /**
     * 实体店铺上传
     *
     * @param map
     * @param store_image_front
     * @param store_image_inside
     * @param store_image_logo
     * @param goods_image
     * @param papers_image_front
     * @param papers_image_behind
     * @param papers_image_Holdfront
     * @param qualifications_image
     * @param business_license
     * @param health_certificate
     * @param other_documents
     * @return
     */
    @Multipart
    @POST("index.php?c=ios&a=entity_merchants")
    Observable<BasicModel> entity_merchants(@PartMap Map<String, RequestBody> map,
                                            @Part MultipartBody.Part store_image_front,
                                            @Part MultipartBody.Part store_image_inside,
                                            @Part MultipartBody.Part store_image_logo,
                                            @Part MultipartBody.Part goods_image,
                                            @Part MultipartBody.Part papers_image_front,
                                            @Part MultipartBody.Part papers_image_behind,
                                            @Part MultipartBody.Part papers_image_Holdfront,
                                            @Part MultipartBody.Part qualifications_image,
                                            @Part MultipartBody.Part business_license,
                                            @Part MultipartBody.Part health_certificate,
                                            @Part MultipartBody.Part other_documents);


    /**
     *  虚拟商家
     * @param map
     * @param store_image_logo
     * @param papers_image_front
     * @param papers_image_behind
     * @param papers_image_Holdfront
     * @return
     */
    @Multipart
    @POST("index.php?c=ios&a=dummy_merchants")
    Observable<BasicModel> dummy_merchants(@PartMap Map<String, RequestBody> map,
                                           @Part MultipartBody.Part store_image_logo,
                                           @Part MultipartBody.Part papers_image_front,
                                           @Part MultipartBody.Part papers_image_behind,
                                           @Part MultipartBody.Part papers_image_Holdfront);

    /**
     * 虚拟商家申请
     * @param map
     * @param store_image_logo
     * @return
     */
    @Multipart
    @POST("index.php?c=ios&a=dummy_merchants")
    Observable<BasicModel> dummy_merchants(@PartMap Map<String, RequestBody> map,
                                           @Part MultipartBody.Part store_image_logo);
//
//    /**
//     * 免费领取
//     * @param lng
//     * @param lat
//     * @return
//     */
//    @GET("/index.php?c=ios&a=type_dui")
//    Observable<BasicModel<List<GoodCommonDetailModel>>> typeDui(@Query("lng") String lng,
//                                                                @Query("lat") String lat);


    /**
     * 免费领取 1瓶 价格为0时候 调用
     * @param member_id
     * @param exchange_amount
     * @param order_sn
     * @return
     */
    @GET("/index.php?c=ios&a=exchange")
    Observable<BasicModel> exchange(@Query("member_id") String member_id,
                                    @Query("exchange_amount") String exchange_amount,
                                    @Query("order_sn") String order_sn);


//    /**
//     * 我的优惠
//     *
//     * @param url
//     * @return
//     */
//    @GET
//    Observable<BasicModel<List<DiscountModel>>> discountCoupon(@Url String url);
//
//    /**
//     * 我的收藏
//     *
//     * @param url
//     * @return
//     */
//    @GET
//    Observable<BasicModel<List<CollectGoodModel>>> memberCollect(@Url String url);
//
//
//    /**
//     * 订单详情
//     *
//     * @param url
//     * @return
//     */
//    @GET
//    Observable<BasicModel<OrderMsgModel>> orderDetails(@Url String url);
//
//
//    /**
//     * 虚拟注册套餐显示
//     *
//     * @param member_id
//     * @param lat
//     * @param lng
//     * @return
//     */
//    @GET(IP + "?c=ios&a=setting")
//    Observable<BasicModel<VirtualFirstModel>> virtualShop(@Query("member_id") String member_id,
//                                                          @Query("lat") String lat,
//                                                          @Query("lng") String lng);

    /**
     * 帮我开店
     *
     * @param params
     * @return
     */
    @GET(IP + "?c=ios&a=openShop")
    Observable<BasicModel> openShop(@QueryMap HashMap<String, String> params);

    /**
     * 消费明细
     * @param member_id
     * @return
     */

//    @GET("index.php?c=ios&a=trading")
//    Observable<BasicModel<TradingModel>> trading(@Query("member_id") String member_id);

    @GET("index.php")
    Observable<BasicModel> cartOrder(@Query("c") String c, @Query("a") String a,
                                     @Query("member_id") String member_id);

    /**
     * 首页二维码扫描
     *
     * @param c
     * @param a
     * @param brand_num
     * @return
     */
    @GET("index.php")
    Observable<BasicModel<QrScanResultModel>> qrCode(@Query("c") String c, @Query("a") String a, @Query("brand_num") String brand_num);
//
//
//    /**
//     * 首页轮播
//     * @param type 1 首页轮播
//     *             2 首页图片
//     *             3 商铺首页
//     *             4 商铺图片
//     * @return
//     */
//    @GET("index.php?c=ios&a=Broadcast")
//    Observable<BasicModel<List<ValueModel>>> Broadcast(@Query("type") String type);
//
//    /**
//     * 获取商品详情
//     *
//     * @param c
//     * @param a
//     * @param gid
//     * @return
//     */
//    @GET("index.php")
//    Observable<BasicModel<GoodCommonDetailModel>> getGoodCommonDetaial(@Query("c") String c, @Query("a") String a, @Query("gid") String gid,
//                                                                       @Query("key_words") String key_words);

    @GET("index.php")
    Observable<BasicModel> submitOrder(@Query("c") String c, @Query("a") String a, @Query("data") String data);


//    /**
//     * 订单生成
//     * @param orderadd
//     * @return
//     */
//    @Multipart
//    @POST("index.php?c=ios&a=orderAdd")
//    Observable<BasicModel<OrderModel>> orderAdd(@PartMap Map<String, RequestBody> orderadd);
//
//
//    /**
//     * 底部购物车展示
//     * @param member_id
//     * @param vid
//     * @return
//     */
//    @GET("index.php?c=ios&a=shopgou")
//    Observable<BasicModel<List<CarModel>>> shopgou(@Query("member_id") String member_id,
//                                                   @Query("vid") String vid);
//
//
//    /**
//     * 订单地址选择，和优惠券
//     * @param vid  店铺id
//     * @param member_id 用户id
//     * @return
//     */
//    @GET("index.php?c=ios&a=orderHome")
//    Observable<BasicModel<OrderAddressQuanEntity>> orderHome(@Query("vid") String vid,
//                                                             @Query("member_id") String member_id);
//
//
//
//    @GET("index.php")
//    Observable<BasicModel<List<OrderModel>>> getOrderList(@Query("c") String c, @Query("a") String a,
//                                                          @Query("member_id") String member_id,
//                                                          @Query("type") int type,
//                                                          @Query("page") int page);
//
//    @GET("index.php")
//    Observable<BasicModel<List<HomeSearchModel>>> homeSearch(@Query("c") String c, @Query("a") String a,
//                                                             @Query("key") String key,
//                                                             @Query("type") String type,
//                                                             @Query("lat") String lat,
//                                                             @Query("lng") String lng);

    /**
     * 购物车添加
     *
     * @param c
     * @param a
     * @param gid
     * @param buyid
     * @param goods_ppp
     * @param cart_id 从购物车接口取出来
     * @return
     */
    @GET("index.php")
    Observable<BasicModel> carAdd(@Query("c") String c,
                                  @Query("a") String a,
                                  @Query("gid") String gid,
                                  @Query("buyid") String buyid,
                                  @Query("goods_ppp") String goods_ppp,
                                  @Query("cart_id") String cart_id);


    /**
     * 购物车减少
     *
     * @param c
     * @param a
     * @param gid
     * @param buyid
     * @param goods_ppp
     * @param cart_id 从购物车接口取出来
     * @return
     */
    @GET("index.php")
    Observable<BasicModel> carReduce(@Query("c") String c, @Query("a") String a, @Query("gid") String gid,
                                     @Query("buyid") String buyid, @Query("goods_ppp") String goods_ppp, @Query("cart_id") String cart_id);

    /**
     * 购物车删除
     *
     * @param c
     * @param a
     * @param vid
     * @param member_id
     * @return
     */
    @GET("index.php")
    Observable<BasicModel> carDelete(@Query("c") String c, @Query("a") String a, @Query("vid") String vid, @Query("member_id") String member_id);

//    //    http://47.97.211.84/index.php?c=ios&a=venCritic
//    @GET("index.php")
//    Observable<BasicModel<ShopCommendModel>> getShopCommentList(@Query("c") String c,
//                                                                @Query("a") String a,
//                                                                @Query("vid") String vid,
//                                                                @Query("page") String page,
//                                                                @Query("type") String type);
//
//    // http://47.97.211.84/index.php?c=ios&a=orderDetails
//    @GET("index.php?c=ios&a=orderDetails")
//    Observable<BasicModel<OrderDetailModel>> orderDetail(@Query("order_sn") String order_sn);


    /**
     * http://47.97.211.84/index.php?c=ios&a=jbzVendor
     */
    @GET("index.php")
    Observable<BasicModel> getHomeShop(@Query("c") String c,
                                       @Query("a") String a, @Query("order_sn") String order_sn);


    @GET("index.php")
    Observable<BasicModel> aliPay(@Query("c") String c,
                                  @Query("a") String a,
                                  @Query("order_sn") String order_sn,
                                  @Query("pay_sn") String pay_sn,
                                  @Query("buyer_id") String buyer_id,
                                  @Query("vid") String vid,
                                  @Query("money") String money);

    // http://47.97.211.84/index.php?c=ios&a=orderDate
//    @GET("index.php")
//    Observable<BasicModel2> orderDate(@Query("c") String c,
//                                      @Query("a") String a,
//                                      @Query("order_id") String order_id);

    /**
     * 取消订单
     *
     * @param c
     * @param a
     * @param order_sn
     * @return
     */
    //http://47.97.211.84/index.php?c=ios&a=cancelOrder
    @GET("index.php")
    Observable<BasicModel> cancelOrder(@Query("c") String c,
                                       @Query("a") String a,
                                       @Query("order_sn") String order_sn);


    /**
     * 订单地址录入，确认订单
     *
     * @param c
     * @return
     */
    @FormUrlEncoded
    @POST("index.php?c=ios&a=orderAddress")
    Observable<BasicModel> orderEdit(@FieldMap Map<String, String> c);

//    @GET("index.php")
//    Observable<BasicModel<List<HomeCategoryDetailModel>>> goodFfilter(@QueryMap Map<String, String> c);

//    /**
//     * 查看店铺优惠券
//     *
//     * @param vid
//     * @param member_id
//     * @return
//     */
//    @GET("index.php?c=ios&a=coupons")
//    Observable<BasicModel<List<CouponCenterModel>>> queryShopCoupon(@Query("vid") String vid, @Query("member_id") String member_id);

    @GET("index.php?c=ios&a=receives")
    Observable<BasicModel> takeCoupons(@QueryMap Map<String, String> c);

    //http://47.97.211.84/index.php?c=ios&a=share&member_id=102

//    /**
//     * 分享个数
//     *
//     * @param member_id
//     * @return
//     */
//    @GET("index.php?c=ios&a=share")
//    Observable<BasicModel<ShareNumModel>> getShareCount(@Query("member_id") String member_id);

    /**
     * 积分兑换
     *
     * @param member_id
     * @return
     */
    @GET("index.php?c=ios&a=exchange")
    Observable<BasicModel<Map<String, Integer>>> exchange(@Query("member_id") String member_id);

    /**
     * 添加收藏
     *
     * @param member_id
     * @param vid
     * @param gid
     * @param store_name
     * @param goods_name
     * @param goods_image
     * @param log_price
     * @return
     */
    @GET("index.php?c=ios&a=favorites")
    Observable<BasicModel> addFav(@Query("member_id") String member_id,
                                  @Query("vid") String vid,
                                  @Query("gid") String gid,
                                  @Query("store_name") String store_name,
                                  @Query("goods_name") String goods_name,
                                  @Query("goods_image") String goods_image,
                                  @Query("log_price") String log_price);

    /**
     * 取消收藏
     *
     * @param member_id
     * @param gid
     * @return
     */
    @GET("index.php?c=ios&a=unfavorites")
    Observable<BasicModel> deleteFav(@Query("member_id") String member_id,
                                     @Query("gid") String gid
    );

//    /**
//     * 店铺页面商品搜索
//     *
//     * @param vid
//     * @param key
//     * @return
//     */
//    @GET("index.php?c=ios&a=goods_search")
//    Observable<BasicModel<List<ShopHomeListModel.ShopHomeGoodModel>>> shopSearch(@Query("vid") String vid,
//                                                                                 @Query("key") String key);
//
//    /**
//     * 余额支付
//     * @param order_sn
//     * @param buyer_id
//     * @param vid
//     * @param money
//     * @param vidstate
//     * @return
//     */
//    @GET("index.php?c=ios&a=balancePay") Observable<PayModel> balancePay(@Query("order_sn") String order_sn,
//                                                                         @Query("buyer_id") String buyer_id,
//                                                                         @Query("vid") String vid,
//                                                                         @Query("money") String money,
//                                                                         @Query("vidstate") String vidstate);

//    /**
//     * 推荐管理
//     * @param member_id
//     * @return
//     */
//    @GET("/index.php/home/ios/fictitious")
//    Observable<BasicModel<FictitiousModel>> fictitious(@Query("member_id") String member_id);


    /**
     * 邀请码是否正确
     * @param referee_num 邀请码
     * @return
     */
    @FormUrlEncoded
    @POST("index.php?c=ios&a=Invitation")
    Observable<BasicModel> invitation(@Field("referee_num") String referee_num);


//    /**
//     * 猜你喜欢
//     * @param lat
//     * @param lng
//     * @return
//     */
//    @GET("index.php?c=ios&a=jbzLike")
//    Observable<BasicModel<List<CoinsModel>>> jbzLike(@Query("lat") String lat, @Query("lng") String lng);

    /**
     * 充值
     * @param map 卖家
//     * @param money 价格
     * @return
     */
    @GET("/index.php?c=ios&a=balPay")
    Observable<BasicModel> balPay(@QueryMap Map<String, String> map);


//    @GET("index.php?c=ios")
//    Observable<BasicModel<List<CoinsModel>>> coins(@Query("a") String a, @Query("lat") String lat,
//                                                   @Query("lng") String lng, @Query("gc_id") String gc_id);


}
