package com.ryx.rxretrofitdemo.base;

public interface RefreshAbleFragment {
    void onRefreshComplete();

    void onRefresh();

    void clearData();
}
