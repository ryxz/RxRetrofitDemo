package com.ryx.rxretrofitdemo.base;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.ryx.rxretrofitdemo.MainActivity;
import com.ryx.rxretrofitdemo.R;
import com.ryx.rxretrofitdemo.http.HttpManager;
import com.ryx.rxretrofitdemo.http.HttpService;
import com.trello.rxlifecycle2.components.support.RxAppCompatActivity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.ObservableTransformer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;


public class BaseActivity extends RxAppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);//保持竖屏
    }

     @Override
    public void onContentChanged() {
        super.onContentChanged();
        registerEventBus();
    }

    public <T> Fragment addFragment(Class<T> clazz) {
        Fragment fragment = null;
        getSupportFragmentManager().beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .add(R.id.fragment_container, fragment = Fragment.instantiate(this, clazz.getName()))
                .addToBackStack(clazz.getName()).commitAllowingStateLoss();
        return fragment;
    }

    public <T> Fragment addFragment(Class<T> clazz, Bundle bundle) {
        Fragment fragment = null;
        getSupportFragmentManager().beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .add(R.id.fragment_container, fragment = Fragment.instantiate(this, clazz.getName(), bundle))
                .addToBackStack(clazz.getName()).commitAllowingStateLoss();
        return fragment;
    }

    public <T> Fragment switchFragment(int id, Class<T> clazz) {
        Fragment fragment = null;
        getSupportFragmentManager().beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .replace(id, fragment = Fragment.instantiate(this, clazz.getName())).commitAllowingStateLoss();
        return fragment;
    }

    public <T> Fragment switchFragment(int id, Class<T> clazz, Bundle bundle) {
        Fragment fragment = null;
        getSupportFragmentManager().beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .replace(id, fragment = Fragment.instantiate(this, clazz.getName(), bundle)).commitAllowingStateLoss();
        return fragment;
    }

    public <T> Fragment attacheFragment(int id, Class<T> clazz, Bundle bundle) {
        Fragment fragment = null;
        getSupportFragmentManager().beginTransaction()
                .replace(id, fragment = Fragment.instantiate(this, clazz.getName(), bundle)).commitAllowingStateLoss();
        return fragment;
    }

    protected HttpService getHttpService() {
        return HttpManager.get().getHttpService();
    }

    public <T> ObservableTransformer<T, T> apply() {
        return new ObservableTransformer<T, T>() {
            @Override
            public ObservableSource<T> apply(Observable<T> observable) {
                ObservableTransformer<T, T> transformer = bindToLifecycle();
                return observable.subscribeOn(Schedulers.io()).doOnSubscribe(new io.reactivex.functions.Consumer<Disposable>() {
                    @Override
                    public void accept(Disposable disposable) throws Exception {
                        if (!isNetworkConnected(BaseActivity.this)) {
                            Toast.makeText(BaseActivity.this, "网络未连接，请检查网络后重试", Toast.LENGTH_SHORT).show();
                        }
                    }
                }).compose(transformer).observeOn(AndroidSchedulers.mainThread());
            }
        };
    }

    public <T> ObservableTransformer<T, T> applyNoLifeCycle() {
        return new ObservableTransformer<T, T>() {
            @Override
            public ObservableSource<T> apply(Observable<T> observable) {
                return observable.subscribeOn(Schedulers.io()).doOnSubscribe(new io.reactivex.functions.Consumer<Disposable>() {
                    @Override
                    public void accept(Disposable disposable) throws Exception {
                        if (!isNetworkConnected(BaseActivity.this)) {
                            Toast.makeText(BaseActivity.this, "网络未连接，请检查网络后重试", Toast.LENGTH_SHORT).show();
                        }
                    }
                }).observeOn(AndroidSchedulers.mainThread());
            }
        };
    }

    @Subscribe
    public void onEvent(BaseActivity obj) {
    }

    /**
     * 判断是否有网络连接
     *
     * @param context
     * @return
     */
    public static boolean isNetworkConnected(Context context) {
        boolean isConnect = false;
        ConnectivityManager conManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo network = conManager.getActiveNetworkInfo();
        if (network != null) {
            isConnect = conManager.getActiveNetworkInfo().isAvailable();
        }
        return isConnect;
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterEventBus();
    }

    protected void registerEventBus() {
        EventBus.getDefault().register(this);
    }

    protected void unregisterEventBus() {
        EventBus.getDefault().unregister(this);
    }

    public void setOnClickListener(int id, View.OnClickListener clickListener) {
        findViewById(id).setOnClickListener(clickListener);
    }

    public void setText(int id, String text) {
        ((TextView) findViewById(id)).setText(text);
    }

    public String getTextStr(int id) {
        return ((TextView) findViewById(id)).getText().toString().trim();
    }

    public void launch(Class clazz) {
        startActivity(new Intent(this, clazz));
    }

//    @Subscribe
//    public void onEvent(ShowCarEvent event) {
//        if (this instanceof MainActivity) {
//            return;
//        }
//        finish();
//    }
}
