package com.ryx.rxretrofitdemo.base;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import static android.view.View.GONE;


/**
 * Created by yuanc on 2016/8/16.
 */
public abstract class BaseRecyclerAdapter<Holder extends RecyclerView.ViewHolder, Data> extends RecyclerView.Adapter<Holder> {

    /**
     * 数据
     */
    private List<Data> mList;
    /**
     * 空布局
     */
    private View mEmptyView;

    public BaseRecyclerAdapter(List<Data> list) {
        this.mList = list;
    }

    public BaseRecyclerAdapter(List<Data> list, View emptyView) {
        this.mList = list;
        this.mEmptyView = emptyView;
        if (emptyView != null) {
            emptyView.setVisibility(list == null || list.size() == 0 ? View.VISIBLE : GONE);
        }
    }

    public void setList(List<Data> list) {
        mList = list;
    }

    public BaseRecyclerAdapter() {
        this.mList = new ArrayList<>();
    }

    /**
     * 刷新数据
     *
     * @param list：内容
     */
    public void refresh(List<Data> list) {
        this.mList = list;
        notifyDataSetChanged();
    }

    /**
     * 加载更多
     *
     * @param list
     */
    public void loadMore(List<Data> list) {
        this.mList.addAll(list);
        notifyDataSetChanged();
    }

    public void setEmptyView(View emptyView) {
        this.mEmptyView = emptyView;
    }

    /**
     * 清空数据
     */
    public void clear() {
        if (mList != null) {
            mList.clear();
            notifyDataSetChanged();
        }
        if (mEmptyView != null) {
            mEmptyView.setVisibility(View.VISIBLE);
        }
    }


    public List<Data> getList() {
        return mList;
    }

    public Data getItem(int position) {
        if (position >= 0 && position <= mList.size()) {
            return mList.get(position);
        } else {
            return null;
        }
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        parent.setClickable(true);
        View itemView = null;
        int itemLayout = getItemLayout(viewType);
        if (itemLayout != 0) {
            itemView = LayoutInflater.from(parent.getContext()).inflate(itemLayout, parent, false);
        }
        return getHolder(itemView, viewType);
    }

    @Override
    public void onBindViewHolder(final Holder holder, final int position) {
        if (mEmptyView != null) {
            mEmptyView.setVisibility(GONE);
        }
        handleClickListener(holder, position);
        if (position < mList.size()) {
            bindView(holder, mList.get(position), position);
        } else {
            bindView(holder, null, position);
        }
    }

    protected void handleClickListener(final Holder holder, final int position) {
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getOnItemClickListener() != null) {
                    onItemClickListener.onItemClick(holder.itemView, position);
                }
            }
        });
    }

    protected abstract int getItemLayout(int viewType);

    protected abstract void bindView(Holder holder, Data data, int position);

    protected abstract Holder getHolder(View itemView, int viewType);

    @Override
    public int getItemCount() {
        int count = mList == null ? 0 : mList.size();
        if (mEmptyView != null && count == 0) {
            mEmptyView.setVisibility(View.VISIBLE);
        }
        return count;
    }

    private OnItemClickListener onItemClickListener;

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public OnItemClickListener getOnItemClickListener() {
        return onItemClickListener;
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }
}
