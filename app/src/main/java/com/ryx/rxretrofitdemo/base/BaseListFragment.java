package com.ryx.rxretrofitdemo.base;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import com.ryx.rxretrofitdemo.R;

public abstract class BaseListFragment<Data, VH extends BaseViewHolder> extends BaseFragment implements RefreshAbleFragment {
    private RecyclerView mRecyclerView;
    private BaseQuickAdapter<Data, VH> mQuickAdapter;
    SwipeRefreshLayout mRefreshLayout;
    private BaseRecyclerAdapter<VH, Data> mBaseRecyclerAdapter;

    public BaseRecyclerAdapter<VH, Data> getBaseRecyclerAdapter() {
        return mBaseRecyclerAdapter;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View contentView = null;
        if (onGetLayout() != -1) {
            contentView = inflater.inflate(onGetLayout(), container, false);
            mRefreshLayout = onGetRefreshLayout(contentView);
            if (mRefreshLayout == null) {
                throw new IllegalStateException("Please override onGetRefreshLayout() method and init SwipeRefreshLayout");
            }
            mRecyclerView = onGetRecyclerView(contentView);
            if (mRecyclerView == null) {
                throw new IllegalStateException("Please override onGetRecyclerView() method and init RecyclerView");
            }
            mRecyclerView.setLayoutManager(getLayoutManager());
        } else {
            mRefreshLayout = new SwipeRefreshLayout(getActivity());
            mRefreshLayout.setLayoutParams(new ViewGroup.MarginLayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            mRecyclerView = new RecyclerView(getActivity());
            mRecyclerView.setLayoutParams(new ViewGroup.MarginLayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            mRecyclerView.setLayoutManager(getLayoutManager());
            mRefreshLayout.addView(mRecyclerView);
            contentView = mRefreshLayout;
        }

        mBaseRecyclerAdapter = onGetAdapter();
        RecyclerView.ItemDecoration itemDecoration = onGetItemDecoration();
        if (itemDecoration != null) {
            mRecyclerView.addItemDecoration(itemDecoration);
        }
        if (mBaseRecyclerAdapter != null) {
            mRecyclerView.setAdapter(mBaseRecyclerAdapter);
        } else {
            mRecyclerView.setAdapter(mQuickAdapter = new BaseQuickAdapter<Data, VH>(getItemLayoutId()) {
                @Override
                protected void convert(VH helper, Data item) {
                    cover(helper, item);
                }

                @Override
                protected VH createBaseViewHolder(View view) {
                    VH vh = BaseListFragment.this.createBaseViewHolder(view);
                    if (vh != null) {
                        return vh;
                    }
                    return super.createBaseViewHolder(view);
                }
            });
            mQuickAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                    BaseListFragment.this.onItemClick(adapter, view, position);
                }
            });
        }
        mRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                BaseListFragment.this.onRefresh();
            }
        });
        return contentView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getRefreshLayout().setRefreshing(true);
        onRefresh();
    }

    public VH createBaseViewHolder(View view) {
        return null;
    }

    public void onRefreshComplete() {
        mRefreshLayout.setRefreshing(false);
    }

    protected RecyclerView.ItemDecoration onGetItemDecoration() {
        return null;
    }

    public RecyclerView getRecyclerView() {
        return mRecyclerView;
    }

    protected void onItemClick(BaseQuickAdapter adapter, View view, int position) {

    }

    public SwipeRefreshLayout getRefreshLayout() {
        return mRefreshLayout;
    }

    protected int onGetLayout() {
        return getLayoutId();
    }

    public void showEmpty(boolean isShow) {
        View iv = findViewById(R.id.iv_empty);
        View tv = findViewById(R.id.tv_no_data);
        if (iv != null) {
            iv.setVisibility(isShow ? View.VISIBLE : View.GONE);
        }
        if (tv != null) {
            tv.setVisibility(isShow ? View.VISIBLE : View.GONE);
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_list;
    }

    protected SwipeRefreshLayout onGetRefreshLayout(View contentView) {
        return contentView.findViewById(R.id.srl);
    }

    protected RecyclerView onGetRecyclerView(View contentView) {
        return contentView.findViewById(R.id.rv);
    }

    public void setOnRefreshListener(@Nullable SwipeRefreshLayout.OnRefreshListener listener) {
        mRefreshLayout.setEnabled(true);
        mRefreshLayout.setOnRefreshListener(listener);
    }

    public abstract void onRefresh();

    public BaseQuickAdapter<Data, VH> getQuickAdapter() {
        return mQuickAdapter;
    }

    /**
     * 如果你需要自定义一个Adapter，则重写此方法
     * 否则使用默认的模版来创建一个Adapter
     *
     * @return
     */
    protected BaseRecyclerAdapter<VH, Data> onGetAdapter() {
        return null;
    }

    protected abstract int getItemLayoutId();

    protected abstract void cover(VH helper, Data item);

    protected RecyclerView.LayoutManager getLayoutManager() {
        return new LinearLayoutManager(getActivity());
    }

    @Override
    public void clearData() {
        if (getQuickAdapter() != null) {
            getQuickAdapter().setNewData(null);
        } else if (getBaseRecyclerAdapter() != null) {
            getBaseRecyclerAdapter().clear();
        }
    }
}
