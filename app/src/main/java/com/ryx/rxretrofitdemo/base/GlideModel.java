package com.ryx.rxretrofitdemo.base;

import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.module.AppGlideModule;

/**
 * Created by Ryx on 2019/1/7.
 */
@GlideModule
public class GlideModel extends AppGlideModule {
}
